package fr.dw.progicamobile

import IGeoAPI
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

class ClientGeoAPI() {
    var retrofit: Retrofit =
        Retrofit.Builder().baseUrl(API_URL).addConverterFactory(MoshiConverterFactory.create())
            .build()
    var service = retrofit.create(IGeoAPI::class.java)

    companion object {
        var API_URL = "https://geo.api.gouv.fr/"
    }

}