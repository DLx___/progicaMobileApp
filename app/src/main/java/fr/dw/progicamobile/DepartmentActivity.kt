package fr.dw.progicamobile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import fr.dw.progicamobile.job.Department
import kotlinx.android.synthetic.main.activity_department.*
import retrofit2.Call
import retrofit2.Response

class DepartmentActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar!!.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_department)
        val myIntent = intent
        val idRegion = myIntent.getIntExtra("region", 0)
        val clientGeoAPI = ClientGeoAPI()

        clientGeoAPI.service.getDepartementsOfRegion(idRegion)
            .enqueue(object : retrofit2.Callback<List<Department>> {
                override fun onResponse(
                    call: Call<List<Department>>,
                    response: Response<List<Department>>
                ) {
                    val departments = response.body()
                    departments?.let {
                        showDepartments(ArrayList(departments))
                    }
                }

                override fun onFailure(call: Call<List<Department>>, t: Throwable) {
                    Log.e("REG", "Error : $t")
                }
            })
    }

    private fun showDepartments(departments: ArrayList<Department>) {
        val adapter =
            ArrayAdapter<Department>(this, android.R.layout.simple_list_item_1, departments)
        listViewDepartment.adapter = adapter
        listViewDepartment.setOnItemClickListener { parent, _, position, _ ->
            val departmentSelected = parent.getItemAtPosition(position) as Department
            Toast.makeText(
                applicationContext,
                "$departmentSelected selected ! ",
                Toast.LENGTH_SHORT
            ).show()
        }

    }
}
