import fr.dw.progicamobile.job.Department
import fr.dw.progicamobile.job.Region
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface IGeoAPI {

    companion object {
        val ENDPOINT = "https://geo.api.gouv.fr/"
    }

    @GET("regions")
    fun getRegions(): Call<List<Region>>

    @GET("regions/{id}/departements")
    fun getDepartementsOfRegion(@Path("id") id: Int) : Call<List<Department>>
}