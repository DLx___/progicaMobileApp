package fr.dw.progicamobile

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import fr.dw.progicamobile.job.Region
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar!!.hide()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        val clientGeoAPI = ClientGeoAPI()

        clientGeoAPI.service.getRegions().enqueue(object : Callback<List<Region>> {
            override fun onResponse(
                call: Call<List<Region>>,
                response: Response<List<Region>>
            ) {
                val regions = response.body()
                regions?.let {
                    afficherRegions(ArrayList(regions))
                }
            }
            override fun onFailure(call: Call<List<Region>>, t: Throwable) {
                Log.e("REG", "Error : $t")
            }
        })
    }

    fun afficherRegions(regions: ArrayList<Region>) {
        val personnesAdapter =
            ArrayAdapter<Region>(this, android.R.layout.simple_list_item_1, regions)
        listViewRegion.adapter = personnesAdapter
        listViewRegion.setOnItemClickListener { parent, _, position, _ ->

            val regionSelected = parent.getItemAtPosition(position) as Region
            Toast.makeText(
                applicationContext,
                "$regionSelected sélectionné !",
                Toast.LENGTH_SHORT
            ).show()
            var myIntent = Intent(this, DepartmentActivity::class.java)
            myIntent.putExtra("region", regionSelected.code)
            startActivity(myIntent)
        }
    }
}