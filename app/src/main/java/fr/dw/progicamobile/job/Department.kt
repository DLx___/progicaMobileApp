package fr.dw.progicamobile.job


data class Department(var code: Int, var nom: String, var codeRegion: Int)  {
    override fun toString(): String {
        return nom
    }
}