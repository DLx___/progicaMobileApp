package fr.dw.progicamobile.job

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Region(var code: Int, var nom: String):Parcelable{
    override fun toString(): String {
        return nom
    }
}